from all2miniseed import Traces2MSeed

tracesmseed = Traces2MSeed("prueba")


print("="*40)
print("Stats:")
tracesmseed.show_stats()

traces = tracesmseed.get_traces()

[print(t) for t in traces]
