from networktools.ssh import bridge, kill

# Bridge to database collector
local_port = 5010
port = 5432
host = 'atlas.csn.uchile.cl'
user = 'geodesia'
ssh_bridge = bridge(local_port, port, host, user)
