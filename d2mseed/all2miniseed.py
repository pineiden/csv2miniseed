import numpy as np
from obspy import UTCDateTime, read, Trace, Stream
import csv
from data_station import stations_llh
import os
from copy import copy
import simplejson as json

def get_csv(folder):
    lista = os.listdir(folder)
    iscsv = lambda x: x if x[-4:]==".csv" else False
    csvs = [iscsv(file) for file in lista]
    if False in csvs:
        csvs.remove(False)
    return csvs

class CSV2Trace:
    header = ["datetime",
              "dt_recv",
              "E",
              "N",
              "U",
              "EE",
              "EN",
              "EU",
              "NN",
              "NU",
              "UU"]
    channel_name = dict(zip(["E", "N", "U", "EE", "NN", "UU"],
                        ('E','N','U','O','P','Q')))
    stats = {'network': 'GPS',
             'station': '',
             'location': '',
             'channel': 'GLx',
             'npts': len([]),
             'sampling_rate': 1,
             'mseed': {'dataquality': 'D'},
             'starttime': None}

    def __init__(self, station_file,
                 folder='csv',
                 *andalaargs,
                 **kehuea):
        self.filename = "./%s/%s" % (folder, station_file)
        station, self.extension = station_file.split('.')
        self.station = station[-4:]
        self.set_stats()
        self.trace = self.calc_trace()

    def set_stats(self):
        # set station
        self.stats.update({'station': self.station})
        # 

    def calc_trace(self):
        data  = {}
        data2 = {}
        count = {}
        stats = {}
        for chann in self.channel_name:
            data.update({chann: list()})
            count.update({chann: 0})
        starttime = {}
        do = None
        traces = []
        new_value = 0
        with open(self.filename, 'r') as file:
            reader = csv.DictReader(file, fieldnames=self.header)
            for row in reader:
                for chann, value in self.channel_name.items():
                    [print(type(v)) for k,v in row.items()]
                    pre_form = "{%s}\n" %chann 
                    #str_row = pre_form.format(**row).replace(',',' ')
                    print(row)
                    if count[chann]>0:
                        new_value = float(row[chann])
                        data[chann].append(new_value)
                    if count[chann] == 1:
                        dt = row.get('datetime').replace(' ', 'T')
                        starttime.update({chann: UTCDateTime(dt)})
                        count[chann] += 1
                    else:
                        count[chann] += 1
        #print("Channel name")
        #print(self.channel_name)
        my_str  = copy(self.stats['channel'])
        for chann in data:
            change = copy(my_str)
            self.stats.update({
                'location':"%.1f,%.1f" %(stations_llh[self.station]['lat'], stations_llh[self.station]['lon']),
                'npts': len(data[chann]),
                'starttime': starttime[chann],
                'channel': change.replace('x', self.channel_name[chann])})
            #data2[chann] = np.concatenate(data[chann])
            #print("chan %s stats %s" %(chann, self.stats))
            #print(self.stats)
            trace = Trace(data=np.array(data[chann]), header=self.stats)
            traces.append(trace)
            del change
        self.stats['channel'] =  copy(my_str)
        return traces

    def get_trace(self):
        return self.trace


class Traces2MSeed:
    def __init__(self, event_name,
                 station_list=None,
                 inputfolder='csv',
                 output='mseed',
                 *andalaargs,
                 **kwea):
        self.output = output
        self.event_name = "gps_%s" % event_name
        self.fileout = "./%s/%s.mseed" % (self.output, self.event_name)
        traces = []
        if not station_list:
            station_list = get_csv(inputfolder)
        for station_file in station_list:
            settrace = CSV2Trace(station_file, folder=inputfolder).get_trace()
            list(map(traces.append, settrace))
        self.traces = traces
        self.st = Stream(traces)
        self.show_stats()
        self.save_traces()

    def save_traces(self):
        self.st.write(self.fileout,
                      format='MSEED',
                      reclen=512)

    def show_stats(self):
        [print(trace.stats) for trace in self.st]

    def get_stats(self):
        return [trace.stats for trace in self.st]

    def get_traces(self):
        return [trace for trace in self.st]
