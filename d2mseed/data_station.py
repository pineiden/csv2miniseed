from orm_collector.manager import SessionCollector
from networktools.geo import ecef2llh

stations = {}
try:
    session = SessionCollector()
    u = session.get_station_data()
    session.close()
    [stations.update({st[0]: {'x':float(st[4]),
                              'y':float(st[5]),
                              'z':float(st[6])}}) for st in u]
    print(stations)
except Exception as exec:
    print("Exception %s" %exec)
    print("Error en conexión a database")

print("#"*40)
print("STATIONS:")
[print(s+"->"+str(v)) for s, v in stations.items()]
print("#"*40)
print("Converting to lat lon h")
stations_llh = {}

[stations_llh.update({s:
                      dict(
                          zip(
                              ('lon', 'lat', 'h'),
                              (ecef2llh(v.get('x'),
                                        v.get('y'),
                                        v.get('z'))
                              )
                          )
                      )
                      }) for s, v in stations.items()]

[print(s+"->"+str(v)) for s, v in stations_llh.items()]
